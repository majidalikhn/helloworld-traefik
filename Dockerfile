FROM openjdk:11
MAINTAINER "majidalikhn@gmail.com"
ADD "target/demo-1-SNAPSHOT.jar" "demo-1-SNAPSHOT.jar"
EXPOSE 8080
CMD ["java", "-jar", "demo-1-SNAPSHOT.jar", "com.treafik.demo.DemoApplication"]
